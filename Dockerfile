FROM node:10

#going to work on a directory named app
WORKDIR /usr/scr/app

#copying package.json and package-lock.json
COPY package*.json ./

#installing dependencies
RUN npm install

#copying files and directories of the project into the workdir
COPY . .
 

#using port 4242
EXPOSE 4242

#command used to run the project
CMD ["node","index.js","concrete-game"]
