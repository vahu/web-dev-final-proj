"use strict"

//setting the configurations for the game
var config = {
  type: Phaser.AUTO,
  width: 1200,
  height:600,
  physics:{
    default:'arcade',
    arcade:{
      gravity:{ y : 400},
      debug: false
    }
  },
  //defining the functions used by the scene
  scene:{
    preload: preload,
    create: create,
    update: update
  }
};

//initializing the game
var game = new Phaser.Game(config);

//declaring the used variables
var player;
var platforms;
var candies;
var cursors;
var snowMen;

var score =0;
var scoreText;
var music;
var collects;

//preload function, where we load every element used in the game
function preload(){
  this.load.image('BG', 'assets/BG.png');
  this.load.image('ground','assets/platform.png');
  this.load.image('candy', 'assets/candy.png');
  this.load.image('cookie', 'assets/cookie.png');
  this.load.image('snowMan', 'assets/SnowMan.png');
  this.load.spritesheet('santa','assets/santa2.png',{ frameWidth: 32, frameHeight: 48 });
  this.load.audio('joy','assets/audio/joy.mp3');

}

//function to create the game using the elemets from preload()
function create(){

//here we declare the variable for the music, which plays during the game
  var music = this.sound.add('joy');
  music.play();
//we add the background image
  this.add.image(0,0,'BG').setOrigin(0,0);
//we declare some static groups, these items won't move or dissappear
  platforms = this.physics.add.staticGroup();
  snowMen = this.physics.add.staticGroup();


//here we create some platforms on our game
  platforms.create(400,568,'ground').setScale(4).refreshBody();
  platforms.create(200,350,'ground');
  platforms.create(750,450,'ground');
  platforms.create(700, 230,'ground');
  platforms.create(100,180,'ground');
  platforms.create(1000,100,'ground');


//here we create some snowmen which are going to be obstacles for our santa
  snowMen.create(340,490,'snowMan');
  snowMen.create(300,317,'snowMan');
  snowMen.create(700,420,'snowMan');

//we add the santa as player on the game
  player = this.physics.add.sprite(100,400,'santa');
  player.setBounce(0.2);
  player.setCollideWorldBounds(true);

//here we create the movements of santa based on the sprite
  this.anims.create({
    key: 'left',
    frames: this.anims.generateFrameNumbers('santa',{start:0,end:3}),
    frameRate: 10,
    repeat:-1
  });

  this.anims.create({
    key:'turn',
    frames:[{key: 'santa',frame:4}],
    frameRate:20
  });

  this.anims.create({
      key: 'right',
      frames:this.anims.generateFrameNumbers('santa',{start:6,end:9}),
      frameRate:10,
      repeat:-1
  });

//initializing the collectibles obects' group
  collects = this.physics.add.group();

//below we send some collectible objects in the game
  var zeroes = [];
  for(var i = 0; i < 3; i++) {
    var x;
    do{
      x = Math.round(Math.random() * 15);
    }while(zeroes.indexOf(x) > -1);
    zeroes[zeroes.length] = x;
  }
  for(var i = 1; i < (1200/ 50); i++) {
    if(zeroes.indexOf(i) < 0){
      var collect = collects.create(i*50, 0, "candy");
    }
    else {
      var collect = collects.create(i*50, 0, "cookie");
    }

    collect.setBounceY(Phaser.Math.FloatBetween(0.1,0.4));

  }


  scoreText = this.add.text(16,16,'score: 0',{fontSize: '32px',fill:'#000'});

  this.physics.add.collider(player,platforms);
  this.physics.add.collider(collects,platforms);
  this.physics.add.collider(player,snowMen)
  this.physics.add.overlap(player,collects,collecting,null,this);
  cursors = this.input.keyboard.createCursorKeys();
  }

//in here the game updates itself at every keyboard input which allows our santa to move
function update(){

  if (cursors.left.isDown){
    player.setVelocityX(-160);

    player.anims.play('left', true);
  }
  else if (cursors.right.isDown){
    player.setVelocityX(160);
    player.anims.play('right', true);
  }
  else{
    player.setVelocityX(0);
    player.anims.play('turn');
  }
  if (cursors.up.isDown && player.body.touching.down)
  {
    player.setVelocityY(-330);
  }

}

//this is the function to add to score when the player collects an item, it also checks if there is still collectibles and if there are not, it throws some more
function collecting(player,collect){
  collect.disableBody(true,true);
  //if there are no active collectible objects, we bounce a new vawe of them
  if (collects.countActive(true)=== 0){
    collects.children.iterate(function (child){
      child.enableBody(true,child.x,0,true,true);
    });
  }

//if the collected item is a candy, we add 10 to the score
  if(collect.key === "candy"){
    score += 10;
  }
  //if the collected item is a cookie, we add 15 to the score as cookies appear less often
  else{
    score += 15;
  }
  scoreText.text = "Score: "+score;


}
