"use strict"
//this file is made to create a countdown till christmas
//note that the file should be updated annually for the following year's christmas
var countdown = document.getElementById("countdown");
var current_date = new Date();
var christmas = new Date("Dec 25 00:00:00 2018");

var seconds = (christmas - current_date)/1000;
var prefixe = "Christmas is in ";

//if the  result of the substraction is negative, christmas has past
if (seconds < 0){
  prefixe = "Christmas passed this amount of time ago ";
  seconds = Math.abs(seconds);
}
//if the substraction result is positive
if(seconds > 0)
{
  var days = Math.floor(seconds/(60*60*24));
  var hours = Math.floor((seconds - (days * 60 * 60 *24))/(60*60));
  var minutes = Math.floor((seconds - ((days * 60 * 60 * 24 + hours * 60 * 60))) / 60);
  var seconds2 = Math.floor(seconds - ((days * 60 * 60 * 24 + hours * 60 * 60 + minutes * 60)));

  countdown.innerHTML = prefixe + days + ' days ' + hours + ' hours ' + minutes + ' minutes and '+ seconds2 + ' seconds ';
}
//if the current date and christmas are exactly the same
else{
  countdown.innerHTML = "Countdown stopped.";
}
